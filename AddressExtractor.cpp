/*
 * AddressExtractor.cpp
 *
 *  Created on: Jan 29, 2021
 *      Author: santiago
 */

/*
 * cUrl example:
 * curl 'https://nominatim.openstreetmap.org/reverse?format=json&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1'
 */


#include "AddressExtractor.h"

const string ADDRESS_KEY("address");
const string ROAD_KEY("road");
const string CITY_KEY("city");
const string STATE_KEY("state");
const string COUNTRY_KEY("country");


Address AddressExtractor::addressFrom(const string &json)
{
	Address add;
	if(reader.parse(json, value))
	{
		if(value.isMember(ADDRESS_KEY))
		{
			value = value[ADDRESS_KEY];
			add.road = value[ROAD_KEY].asString();
			add.city = value[CITY_KEY].asString();
			add.state= value[STATE_KEY].asString();
			add.country = value[COUNTRY_KEY].asString();
		}
	}
	return add;
}




