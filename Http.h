/*
 * Http.h
 *
 *  Created on: Feb 3, 2021
 *      Author: santiago
 */

#ifndef HTTP_H_
#define HTTP_H_

#include <string>

class Http
{
public:
	virtual ~Http(){}
	virtual void initialize() = 0;
	virtual std::string get(const std::string &url) const = 0;
};




#endif /* HTTP_H_ */
