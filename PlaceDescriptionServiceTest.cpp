/*
 * PlaceDescriptionServiceTest.cpp
 *
 *  Created on: Feb 3, 2021
 *      Author: santiago
 */


#include "PlaceDescriptionService.h"
#include "Http.h"
#include <gmock/gmock.h>

using namespace ::testing;
using namespace std;

const string VALID_LATITUDE{"52.5487429714954"};
const string VALID_LONGITUDE{"-1.81602098644987"};

class APlaceDescriptionService : public Test
{
};


class HttpStub : public Http
{
public:
	MOCK_METHOD0(initialize,void()); //Override a non-constant method without argument
	MOCK_CONST_METHOD1(get,string(const string&)); //Override a constant method with one argument
};

TEST_F(APlaceDescriptionService,MakesHttpRequestToObtainAddress)
{
	InSequence forceExpectationOrder; //To concern about the order of calls
	HttpStub httpStub;
	string urlStart = "https://nominatim.openstreetmap.org/reverse?format=json&";
	string expectedUrl = urlStart + "lat=" + VALID_LATITUDE + "&lon=" + VALID_LONGITUDE;

	EXPECT_CALL(httpStub,initialize());
	EXPECT_CALL(httpStub,get(expectedUrl));
	PlaceDescriptionService service{&httpStub};

	service.summaryDescription(VALID_LATITUDE, VALID_LONGITUDE);
}

TEST_F(APlaceDescriptionService,FormatsRetrievedAddressIntoSummaryDescription)
{
	NiceMock<HttpStub> httpStub; // To avoid the warning from google mock due the unexpected call of the initialize method
	EXPECT_CALL(httpStub,get(_)) // The '_' character is a wildcard that means "any parameter"
	.WillOnce(Return( // The get() method should be called once and only once
			R"({"address":{
					"road":"Pilkington Avenue",
					"city":"Birmingham",
					"state":"England",
					"country":"United Kingdom"
				}})"));
	PlaceDescriptionService service{&httpStub};
	auto summary = service.summaryDescription(VALID_LATITUDE, VALID_LONGITUDE);
	ASSERT_THAT(summary, Eq("Pilkington Avenue, Birmingham, England, United Kingdom"));
}





