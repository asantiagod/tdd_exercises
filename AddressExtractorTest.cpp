/*
 * AddressExtractorTest.cpp
 *
 *  Created on: Feb 1, 2021
 *      Author: santiago
 */

#include <gmock/gmock.h>
#include "AddressExtractor.h"

using namespace ::testing;
/*
	{
		"address":
		{
			"house_number":"137",
			"road":"Pilkington Avenue",
			"hamlet":"Maney",
			"town":"Sutton Coldfield",
			"village":"Wylde Green",
			"city":"Birmingham",
			"county":"West Midlands Combined Authority",
			"state_district":"West Midlands",
			"state":"England",
			"postcode":"B72 1LH",
			"country":"United Kingdom",
			"country_code":"gb"
		}
	}
*/

class AnAddressExtractor: public Test
{
public:
	AddressExtractor extractor;
};

MATCHER(isEmpty,"Check if an address structure is empty")
{
	return arg.road.empty() &&
		   arg.city.empty() &&
		   arg.state.empty() &&
		   arg.country.empty();
}

TEST_F(AnAddressExtractor,ReturnsEmptyAddressWhenFailParse)
{
	auto add = extractor.addressFrom("Invalid JSON");
	ASSERT_THAT(add,isEmpty());
}

TEST_F(AnAddressExtractor,ReturnsEmptyAddressWhenNoAddressFound)
{
	auto add = extractor.addressFrom(R"({"place_id":110244838})");
	ASSERT_THAT(add,isEmpty());
}

TEST_F(AnAddressExtractor,PopulatesAddressFromValidJson)
{
	string json = R"({
			"address":
			{
				"road":"Pilkington Avenue",
				"city":"Birmingham",
				"state":"England",
				"country":"United Kingdom"
			}
		})";
	auto add = extractor.addressFrom(json);

	EXPECT_THAT(add.road,Eq("Pilkington Avenue"));
	EXPECT_THAT(add.city, Eq("Birmingham"));
	EXPECT_THAT(add.state, Eq("England"));
	EXPECT_THAT(add.country, Eq("United Kingdom"));
}

TEST_F(AnAddressExtractor,DefaultsNonExsistentFieldsToEmpty)
{
	string json = R"({
			"address":
			{
				"road":"Pilkington Avenue",
				"city":"Birmingham",
				"state":"England"
			}
		})";
	auto add = extractor.addressFrom(json);

	ASSERT_THAT(add.country, Eq(""));
}

