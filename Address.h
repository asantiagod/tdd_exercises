/*
 * Address.h
 *
 *  Created on: Jan 29, 2021
 *      Author: santiago
 */

#ifndef ADDRESS_H_
#define ADDRESS_H_

#include <string>

using std::string;

struct Address
{
	string road;
	string city;
	string state;
	string country;

	string summaryDescription() const
	{
		return road + ", " +
			   city + ", " +
			   state + ", " +
			   country;
	}
};

#endif /* ADDRESS_H_ */
