/*
 * PlaceDescriptionService.h
 *
 *  Created on: Feb 3, 2021
 *      Author: santiago
 */

#ifndef PLACEDESCRIPTIONSERVICE_H_
#define PLACEDESCRIPTIONSERVICE_H_

#include "Address.h"

class Http;

class PlaceDescriptionService
{
public:
	PlaceDescriptionService(Http *http);
	string summaryDescription(const string& latitude, const string& longitude);
private:
	Http* http;

	string createGetRequestUrl(const string& latitude, const string& longitude) const;
	string keyValue(const string &key, const string &value) const;
	Address getAddress(const string& url) const;
};



#endif /* PLACEDESCRIPTIONSERVICE_H_ */
