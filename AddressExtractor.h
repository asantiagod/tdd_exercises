/*
 * AddressExtractor.h
 *
 *  Created on: Jan 29, 2021
 *      Author: santiago
 */

#ifndef ADDRESSEXTRACTOR_H_
#define ADDRESSEXTRACTOR_H_

#include "Address.h"
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>

class AddressExtractor
{
public:
	virtual ~AddressExtractor(){}
	Address addressFrom(const string &);

private:
	Json::Value value;
	Json::Reader reader;
};



#endif /* ADDRESSEXTRACTOR_H_ */
