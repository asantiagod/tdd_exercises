/*
 * PlaceDescriptionService.cpp
 *
 *  Created on: Feb 3, 2021
 *      Author: santiago
 */

#include "PlaceDescriptionService.h"
#include "AddressExtractor.h"
#include "Http.h"

PlaceDescriptionService::PlaceDescriptionService(Http *http) : http(http)
{

}

string PlaceDescriptionService::summaryDescription(const string& latitude, const string& longitude)
{
	auto url =  createGetRequestUrl(latitude, longitude);
	auto address = getAddress(url);
	return address.summaryDescription();
}

string PlaceDescriptionService::createGetRequestUrl(const string& latitude, const string& longitude) const
{
	string server = "https://nominatim.openstreetmap.org/";
	string document = "reverse";
	return 	server + document + "?" +
			keyValue("format", "json") + "&" +
			keyValue("lat", latitude) + "&" +
			keyValue("lon", longitude);
}

string PlaceDescriptionService::keyValue(const string &key, const string &value) const
{
	return key + "=" + value;
}

Address PlaceDescriptionService::getAddress(const string& url) const
{
	AddressExtractor extractor;
	http->initialize();
	auto json = http->get(url);
	return extractor.addressFrom(json);
}

